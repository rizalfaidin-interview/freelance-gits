<?php
function getWeightedChar($char, $multiplier = 1) {
    return (strtolower(ord($char)) - 96) * $multiplier;
}

function weightStrings(string $string, array $queries = []) {
    $chars = str_split($string);
    $prevChar = '';
    $charCollection = [];
    foreach ($chars as $key=>$char) {
        if ($prevChar != $char) {
            $charCollection[] = ['char'=>$char, 'multiplier'=>1];
        } else {
            $lastCollection = end($charCollection);
            $lastCollection['multiplier']++;
            $charCollection[array_key_last($charCollection)] = $lastCollection;
            
        }
        $prevChar = $char;
    }

    $results = [];

    foreach ($charCollection as $charKey=>$arrChar) {
        $charWeight = getWeightedChar($arrChar['char'], $arrChar['multiplier']);
        $results[$charKey] = $queries[$charKey] == $charWeight ? "Yes" : "No";
    }

    return $results;
}

print_r(weightStrings('abbcccd', [1, 4, 9, 8])); // return ["Yes", "Yes", "Yes", "No"]
?>