<?php

function isBalanceBrackets($strBracket) {
    $openingBrackets = ['{', '[', '('];
    $closingBrackets = ['}', ']', ')'];
    $brackets = str_split($strBracket);

    $bracketStacks = [];
    foreach ($brackets as $bracket) {
        if ($bracket == ' ') continue;

        // is not brackets family, return false
        if (!in_array($bracket, $openingBrackets) && !in_array($bracket, $closingBrackets)) return false;

        if (in_array($bracket, $openingBrackets)) {
            $key = array_search($bracket, $openingBrackets);
            array_push($bracketStacks, $closingBrackets[$key]);
            continue;
        } else if (in_array($bracket, $closingBrackets)) {
            if (array_pop($bracketStacks) !== $bracket) {
                return "NO";
            }

            // if stacks is already empty after popping
            if (count($bracketStacks) === 0) return "YES";

            continue;
        }
        
        return "YES";
    }
}

echo isBalanceBrackets("") ? "Yes" : "No";

?>