<?php
function getFirstPalindrome($string, $k, $l, $r) {
    if ($string[$l] != $string[$r]) {
        $palin[$l] = $palin[$r] = Max($string[$l], $string[$r]);
        $k--;
    }
    $l++;
    $r--;

    if ($l < $r) return getFirstPalindrome($string, $k, $l, $r);

    return $string;
}

function nextPalindrome($string, $palin, $k, $l, $r) {
    if ($l == $r) {
        if ($k > 0) {
            $palin[$l] = '9';
        }
    }

    if ($palin[$l] < '9') {
        if ($k >= 2 && $palin[$l] == $string[$l] && $palin[$r] == $string[$r]) { 
            $k -= 2;
            $palin[$l] = $palin[$r] = '9';
        } 
            
        else if ($k >= 1 && ($palin[$l] != $string[$l] || $palin[$r] != $string[$r])) {
            $k--;
            $palin[$l] = $palin[$r] = '9';
        }
    }
    $l++;
    $r--;

    if ($l <= $r) return nextPalindrome($string, $palin, $k, $l, $r);

    return $palin;
}

function maxPalindrome($string, $k) {
    $palin = $string;
    $ans = "";
     
    $l = 0;
    $r = strlen($string) - 1;

    $palin = getFirstPalindrome($string, $k, $l, $r);
 
    if ($k < 0) {
        return "Not possible";
    }
 
    $l = 0;
    $r = strlen($string) - 1;

    $palin = nextPalindrome($string, $palin, $k, $l, $r);

    for ($i = 0; $i < strlen($palin); $i++) {
        $ans .= $palin[$i];
    }
    return $ans;
}


print_r(maxPalindrome('43435', 3)); // output: 93939
