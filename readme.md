### Kompleksitas Soal Nomor 3, Balanced Brackets
* Parsing parameter menjadi array dari karakter
* looping setiap karakter
* pengecekkan spasi kosong, apabila karakter adalah spasi kosong, maka dilanjutkan ke looping berikutnya.
* pengecekkan apakah karakter merupakan keluarga dari bracket, apabila karakter bukan merupakan keluarga dari bracket, maka return "No".
* apabila karakter merupakan bracket pembuka, maka menambahkan element array baru pada variable $bracketStacks yang berisi bracket tutup dari indeks tersebut.
* apabila karakter merupakan bracket penutup, maka dilakukan pengecekkan apakah sama dengan value dari indeks terakhir $bracketStacks. Apabila berbeda, maka return "NO". 
* Apabila isi dari $bracketStacks sudah habis, dan pengecekkan semua value dari indeks $bracketStacks tidak ada yang berbeda, maka return "YES"